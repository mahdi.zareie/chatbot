from django.contrib.auth.models import User
from django.db import models
from .managers import WalletManager


class Wallet(models.Model):
    owner = models.OneToOneField(User, on_delete=models.CASCADE, related_name="wallet")
    balance = models.IntegerField()

    objects = WalletManager()


class Transaction(models.Model):
    source = models.ForeignKey(Wallet, related_name="outgoing_transactions", on_delete=models.CASCADE)
    destination = models.ForeignKey(Wallet, related_name="incoming_transactions", on_delete=models.CASCADE)
    value = models.IntegerField()
    date = models.DateTimeField(auto_now_add=True)
