from wallet.models import Wallet


def get_wallet(user, command):
    wallet = Wallet.objects.filter(owner=user).first()
    return "Your wallet balance is {}$".format(wallet.balance)


def send_money(user, command):
    try:
        if len(command.params) != 2:
            raise ValueError("you need to provide destination and amount of money you want to transfer")
        destination, money = command.params
        destination = Wallet.objects.filter(owner__username=destination).first()
        if destination is None:
            raise ValueError("destination does not exists")
        Wallet.objects.deposit(user.wallet, int(money), destination)
        return "{}$ has been transfered from your account to {}".format(int(money), destination.owner.username)
    except ValueError:
        return "the parameters are not valid"
