from django.db import models
from django.db.transaction import atomic


class WalletManager(models.Manager):
    def deposit(self, source, value, destination):
        from wallet.models import Transaction
        if source.balance < value:
            raise ValueError("Your balance is not enough")
        value = abs(value)
        with atomic() as db_transaction:

            source.balance -= value
            source.save()
            Transaction.objects.create(
                source=source,
                destination=destination,
                value=value
            )
            destination.balance += value
            destination.save()

