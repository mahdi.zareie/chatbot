from django.contrib.auth.models import User
from django.db import models
from .managers import MessageManager


class Conversation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    started_at = models.DateTimeField(auto_now_add=True)
    objects = MessageManager()


class Message(models.Model):
    conversation = models.ForeignKey(Conversation, on_delete=models.CASCADE, related_name='messages')
    text = models.TextField()
    from_bot = models.BooleanField()
    date = models.DateTimeField(auto_now_add=True)
