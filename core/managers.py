from django.db import models
from web.command import Command
from web.get_answer import get_answer_provider


class MessageManager(models.Manager):
    def submit_message(self, user, conversation, message):
        from core.models import Message
        Message.objects.create(
            conversation=conversation,
            text=message,
            from_bot=False,
        )
        command = Command(message)
        answer_provider = get_answer_provider(command.command_name)
        Message.objects.create(
            conversation=conversation,
            text=answer_provider(user, command),
            from_bot=True
        )
