from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r"^login$", LoginView.as_view(), name="login"),
    url(r"^conversations/(?P<conversation_id>.+)", ConversationDetailView.as_view(), name="conversation_details"),
]
