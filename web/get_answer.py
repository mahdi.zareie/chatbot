from wallet.commands import get_wallet, send_money


def default_answer(user, command):
    return "What can I do for you?</br>" \
           "You can choose:</br>" \
           "1. View you wallet details using wallet-details</br>" \
           "2. Transfer money to another account using send-money <destination> <money> </br>"


def get_answer_provider(command_name):
    return {
        "wallet-details": get_wallet,
        "send-money": send_money
    }.get(command_name, default_answer)
