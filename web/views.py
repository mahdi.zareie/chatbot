from django import http
from django.contrib.auth import authenticate, login
from django.urls import reverse
from django.views.generic import TemplateView
from core.models import Conversation


def new_conversation(user):
    conversation = Conversation.objects.create(user=user)
    return http.HttpResponseRedirect(
        reverse("conversation_details", kwargs=dict(conversation_id=conversation.id)
                ),
    )


class LoginView(TemplateView):
    template_name = 'login.html'

    def get_context_data(self, **kwargs):
        if 'message' in self.request.GET:
            kwargs['message'] = self.request.GET['message']
        return super(LoginView, self).get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        username = request.POST.get("Username", None)
        password = request.POST.get("Password", None)
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return new_conversation(user)
        url = reverse("login") + "?message={}".format("Username/password is not correct")
        return http.HttpResponseRedirect(url)


class ConversationDetailView(TemplateView):
    template_name = "conversation.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            url = reverse("login") + "?message={}".format("You need to login first")
            return http.HttpResponseRedirect(url)
        try:
            return super(ConversationDetailView, self).dispatch(request, *args, **kwargs)
        except Conversation.DoesNotExist:
            return new_conversation(request.user)

    def get_context_data(self, **kwargs):
        if 'conversation' not in kwargs:
            kwargs['conversation'] = Conversation.objects.get(id=kwargs['conversation_id'])
        return super(ConversationDetailView, self).get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        conversation = Conversation.objects.get(id=kwargs['conversation_id'])
        Conversation.objects.submit_message(
            user=request.user,
            conversation=conversation,
            message=str(self.request.POST['message']).strip()
        )
        return self.render_to_response(
            context=self.get_context_data(conversation=conversation),
        )
