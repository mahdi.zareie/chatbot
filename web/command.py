class Command:
    def __init__(self, command_str):
        tokens = command_str.split(" ")
        self.command_name = tokens[0]
        self.params = tokens[1:]
